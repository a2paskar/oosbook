import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import SearchPage from '../views/SearchPage.vue';
//NOTE: you must do authentication at this end.  AND in the backend TOO!!!!!
//good starting point: https://www.digitalocean.com/community/tutorials/how-to-set-up-vue-js-authentication-and-route-handling-using-vue-router
const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'Login',
		component: Login,
	},
	{
		path: '/register',
		name: 'Registration',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "about" */ '../views/Registration.vue'),
	},
	{
		path: '/home',
		name: 'Home',
		component: Home,
	},
	{
		path: '/search',
		name: 'Search',
		component: SearchPage,
	},
	{
		//dynamic page!
		path: '/user/:id', //user vs. community...
		name: 'ProfilePage',
		props: true,
		component: () => import('../views/ProfilePage.vue'),
	},
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

export default router;
