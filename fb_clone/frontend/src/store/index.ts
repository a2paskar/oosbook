import { createStore } from 'vuex';
import axios from 'axios';

//EVERYTHING SHOULD BE IN THE STORE

interface User {
	email: String;
	password: String;
	loggedIn: Boolean;
	gender: String;
	firstname: String;
	lastname: String;
	user_id: Number;
}
const moduleLogin = {
	state: {
		email: '',
		loggedIn: false,
		password: '',
		gender: '',
		firstname: '',
		lastname: '',
		user_id: 0,
	},
	mutations: {
		handleLogin(state: User, res: any) {
			//TODO: change this..
			if (res.status == 200) {
				state.loggedIn = true;
				(state.email = res.data['email']),
					(state.gender = res.data['gender']),
					(state.firstname = res.data['firstname']),
					(state.lastname = res.data['lastname']);
				state.password = res.data['password'];
				state.user_id = res.data['user_id'];
			} else {
				state.loggedIn = false;
			}
		},
		logout(state: User) {
			state.email = '';
			state.loggedIn = false;
			state.gender = '';
			state.firstname = '';
			state.lastname = '';
			state.password = '';
			state.user_id = 0;
		},
	},
	actions: {
		handleLogin(context: { state: User; commit: Function }) {
			//THIS WORKS!! All I have to do is start node backend and run vue app.  If I wanna communicate, send requests as follows!, Make sure backend has CORS to communicate cross origins

			const url = 'http://localhost:3001/api/users/login';

			axios
				.post(url, {
					email: context.state.email,
					password: context.state.password,
				})
				.then(res => {
					// do something with res
					context.commit('handleLogin', res);
				})
				.catch(err => {
					console.log('Error');
					// catch error
					context.commit('handleLogin', err);
				});
		},
		logout(context: { commit: Function }) {
			context.commit('logout');
		},
	},
	getters: {},
};

//looks like, if the app needs USER info it will access it here...
const moduleRegistration = {
	state: {
		firstName: '',
		lastName: '',
		email: '',
		password: '',
		gender: '',
	},
	mutations: {},
	actions: {
		async handleSubmit(context: {
			state: {
				firstName: String;
				lastName: String;
				email: String;
				password: String;
				gender: String;
			};
		}) {
			//THIS WORKS!! All I have to do is start node backend and run vue app.  If I wanna communicate, send requests as follows!, nothing else.. Make sure backend has CORS to communicate cross origins

			const url = 'http://localhost:3001/api/users/register'; //defined in backend

			const res = await axios.post(url, {
				firstName: context.state.firstName,
				lastName: context.state.lastName,
				email: context.state.email,
				password: context.state.password,
				gender: context.state.gender,
			});
		},
	},
	getters: {},
};

interface stateHome {
	homePosts: Array<Object>;
	newNotifications: Boolean;
	newMessages: Boolean;
	searchResults: '';
	searchedUsers: Array<Object>;
}

const moduleHome = {
	state: {
		homePosts: [],
		newNotifications: false,
		newMessages: false,
		searchResults: '',
		searchedUsers: [],
	},
	mutations: {
		fetchHomePosts(state: stateHome, res: any) {
			if (res.status == 200) {
				console.log('posts: ', res.data);
				state.homePosts = res.data; //list of all posts that should be on user's Home page
			} else {
				state.homePosts = [];
			}
		},
		submitSearch(state: stateHome, res: any) {
			if (res.status == 200) {
				console.log('search results: ', res.data);
				state.searchedUsers = res.data; //list of all posts that should be on user's Home page
			} else {
				state.searchedUsers = [];
			}
		},
	},
	actions: {
		async fetchHomePosts(context: { state: stateHome; commit: Function }) {
			var url = 'http://localhost:3001/api/posts/all/'; //url to get a user from db

			const user_id = moduleLogin['state']['user_id']; //get from login module
			//TODO: simple.  send the id as part of the url.. dynamic url!  and in backend use this userid to get posts...
			url += user_id;
			axios
				.get(url)
				.catch(err => {
					console.log('Error', err);
					context.commit('fetchHomePosts', err);
				})
				.then(res => {
					context.commit('fetchHomePosts', res);
				});
		},
		async submitSearch(context: { state: stateHome; commit: Function }) {
			var url = 'http://localhost:3001/api/users/'; //searchbar should only really find users from db

			url += context.state.searchResults;
			axios
				.get(url)
				.catch(err => {
					console.log('Error', err);
					context.commit('submitSearch', err);
				})
				.then(res => {
					context.commit('submitSearch', res);
				});
		},
	},
	getters: {},
};

interface statePost {
	content: string;
}
const modulePost = {
	state: {
		content: '',
	},
	mutations: {
		handlePostSubmit(state: statePost, res: any) {
			if (res.status == 200) {
				console.log('Post saved successfully');
			} else {
				console.log('Post failed to save');
			}
		},
		deletePost(state: statePost, res: any) {
			if (res.status == 200) {
				console.log('Post deleted successfully');
			} else {
				console.log('Post failed to delete');
			}
		},
	},
	actions: {
		async handlePostSubmit(
			context: { state: statePost; commit: Function },
			payload: any
		) {
			const content_file = payload['contentpic']; //TODO: this picture has to be handled here ~ saved to file structure and the url location should be saved to db

			const profilepic = payload['profilepic'];
			const user_id = moduleLogin['state']['user_id'];
			const author = moduleLogin['state']['firstname'];

			var url = 'http://localhost:3001/api/posts/add'; //url to get a user from db

			let data = new FormData();
			data.append('name', 'profilepic');
			data.append('file', content_file);
			data.append('content', context.state.content);
			data.append('profilepic', profilepic);
			data.append('user_id', user_id.toString());
			data.append('author', author);

			// let config = {
			// 	header: {
			// 		'Content-Type': 'multipart/form-data',
			// 	},
			// };

			const res = await axios.post(url, data);
			// {
			// 	content: context.state.content,
			// 	profilepic: profilepic, //TOOD:
			// 	contentpic: content_file,
			// 	user_id: user_id,
			// 	author: author,
			// }
			context.commit('handlePostSubmit', res);
		},
		async deletePost(
			context: { state: statePost; commit: Function },
			payload: any
		) {
			const post_id = payload['post_id'];
			console.log('deleting post: ', post_id);
			var url = 'http://localhost:3001/api/posts/delete/';
			url += post_id;
			axios
				.delete(url)
				.catch(err => {
					console.log('Error', err);
					context.commit('deletePost', err);
				})
				.then(res => {
					context.commit('deletePost', res);
				});
		},
	},
	getters: {},
};

interface stateProfilePage {
	myPosts: Array<Object>;
}

const moduleProfilePage = {
	state: {
		myPosts: [],
	},
	mutations: {
		fetchMyPosts(state: stateProfilePage, res: any) {
			if (res.status == 200) {
				state.myPosts = res.data; //list of all posts that should be on user's Home page
				console.log(state.myPosts);
			} else {
				state.myPosts = [];
			}
		},
	},
	actions: {
		async fetchMyPosts(
			context: { state: stateProfilePage; commit: Function },
			route: String
		) {
			var url = 'http://localhost:3001/api/posts/'; //url to get a user from db

			const user_id = route.substr(6); //moduleLogin['state']['user_id']; //get from login module
			//TODO: simple.  send the id as part of the url.. dynamic url!  and in backend use this userid to get posts...
			url += user_id;
			axios
				.get(url)
				.catch(err => {
					console.log('Error', err);
					context.commit('fetchMyPosts', err);
				})
				.then(res => {
					context.commit('fetchMyPosts', res);
				});
		},
		async addFriend(
			context: { state: stateProfilePage; commit: Function },
			route: String
		) {
			var url = 'http://localhost:3001/api/users/'; //url to get a user from db
			const user_id = moduleLogin.state.user_id;
			const friend_id = route.substr(6); //moduleLogin['state']['user_id']; //get from login module
			//TODO: simple.  send the id as part of the url.. dynamic url!  and in backend use this userid to get posts...
			url += user_id;
			axios
				.get(url)
				.catch(err => {
					console.log('Error', err);
					context.commit('fetchMyPosts', err);
				})
				.then(res => {
					context.commit('fetchMyPosts', res);
				});
		},
	},
	getters: {},
};

export default createStore({
	modules: {
		login: moduleLogin,
		registration: moduleRegistration,
		home: moduleHome,
		post: modulePost,
		profile: moduleProfilePage,
	},
});

// store.state.a // -> `moduleA`'s state
// store.state.b // -> `moduleB`'s state
