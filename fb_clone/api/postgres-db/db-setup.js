const { Client} = require('pg')
const execSync = require('child_process').execSync;

// This command will create a new database called oosbook if it doesn't already exist
execSync(
    `psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = 'oosbook'" | grep -q 1 || psql -U postgres -c "CREATE DATABASE oosbook"`, 
    { encoding: 'utf-8' }
);
// TODO: run migrations if db is being created for the first time

const client = new Client({
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    dbname: 'oosbook',
})

client.connect(err => {
    if (err) {
      console.error('connection error', err.stack);
    } else {
      console.log('db connected successfully');
    }
});