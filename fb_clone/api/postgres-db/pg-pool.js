const Pool = require('pg').Pool
const { DB_CONNECTION_VARS } = require('../config');
console.log('USING THE FOLLOWING CONNECTION VARS:');
console.log(DB_CONNECTION_VARS);
const pool = new Pool({
    user: DB_CONNECTION_VARS.user,
    host: DB_CONNECTION_VARS.host,
    database: DB_CONNECTION_VARS.name,
    port: DB_CONNECTION_VARS.port,
});

module.exports = pool;