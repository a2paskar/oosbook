declare namespace Express {
    interface Request {
        session: {
            loggedin:Boolean,
            email:string
        };
    }
}