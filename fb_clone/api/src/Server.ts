import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';

import express, { NextFunction, Request, Response } from 'express';
import StatusCodes from 'http-status-codes';
import 'express-async-errors';

import BaseRouter from './routes';
// import logger from '@shared/Logger';

const app = express();
const { BAD_REQUEST } = StatusCodes;
var cors = require('cors');

var session = require('express-session');

const fileUpload = require('express-fileupload'); //for images!

//VERY SIMPLE PATTERN.  express app is the root node just as Vue APP is a root node
//backend has its own set of routes (url + function that does shit when a request is sent to the url) ~ in this case the express app just know what to do when a request is sent to a url.
//

//just like any frontend which has a set of routes are held and used by App object ~ in this case views defined in front end with it's tree of components is displayed

//The App object should just have its router + a table of contents filled with views (pages) ~ frontend

//in the backend.  The app object = Server file will have its router (a bunch of urls related to a page ex) Users.ts has posts and gets related to the regiatrtaion page...?) + all the middleware and libraries  needed to perform its responsibilities in the view functions (associated with a url) compile them together
//in index we call app.listen() which serves itself on a given PORT

//state in the frontend:
//EVERY component has 3 sides: structure (html template), styling (css), functionality (JS)
//since the App object contains all the state variables + state functions of EVERY component the app can respond to changes in the state of any given component
// App can communicate with the backend based on (ONLY ~ only way it communicates) requests sent by frontend=component in tree (frontend app)

//For each stage of the typical app process (there are type of apps as well, in this case a typical social media site/ blog site):
//1) Sign Up
//2) Login
//3) Home page
//4) User profile page (or any other type of page~ checkout any product website)
/*

Sign Up
- frontend
the sign up page would be a view (Vue -> views dir) which is associated with a url based on frontend router compiled to the overall App
the view has the structure of all the components that make up the appropriate page.  Each structure on the page can be apart of the View or be their own individual component
if it's part of the View (a form is on Home.vue), any sort of backend data it needs from backend or external API is the responsibility of the page component itslef
This idea extends in the sense that a html structure or a component existing as parts (in its render/template tag) in a parent component, delegates data responsibility to the parent component
ex) in order for a table tag to updates it's contents, and it part of a page component.  The page component needs get state data from backend for table to function
the more nested a page comes, the more expensive in a way, since instead of basic html elements at a page level, you created components which may also have its own state and possible other components

Sign up page is not a page that necessarily does much in terms of many components.  In fact, the sign up page usually consists of a form (which may be wrapped for resuability only)
//no further nesting needed for components on such a basic page.  It also doesn't require any data from the backend.  

//just needs to post this data to the backend 
//since this data needs to be communicated to the backend.  The sign up page (or generalizable form component that takes in post_url as a prop..)
//is responsible for sending a post request to the backend.  Once data is sent, based on success/failure status of the request (returned by the backend), the page should decide if it wants to switch routes
//~ backend is its own entity since it could easily be an external API instead and this api is the source of all data needed by the frontend app
//in general, the fronend needs to decide WHAT to do after getting and sending information to the backend.  If it's sending information, you use a form with state variables binded to the form inputs.  On a button click, frontend needs to be able to send this information to the backend server
// in hopes that it will take this data, validate it ~ check who's requesting it... and write it to the db
//once backend has successfully done this it will send a success status to frontend onclick method.  if success, frontend switches pages to a route defined on frontend router used by App
//if we're on a new page, the page will either send user info and get user info from db.  So it'll usually submit a get request when it's MOUNTED!!!!!!
//and send this information (after you logged in...) to all the components that need it
//with a state management system: everything is the same except the fact that state is in the store (to display ~ via getters (calculate something based on it) or accessing state directly) the inputs & buttons call commit/ dispatch methods in the store which manipulate state, 


//in general every component has an event listener that will do something based on state changes.  State changes are based on the workflow design of the entire application + user inputs
//note that a component doesn't have to interact with the backend at all.  instead its state can determine what another component does (this later component can decide to get backend data based on this state ) ~ by passing in props
OR 

user behaviours: 

1)change url -> if it exists on the frontend router, then the corresponding page gets displayed
the page then depending on purpose on the page, it will either send data to backend or get data from backend, and do something after this
~note how the user jouenry, the state gets propagated not only downwards (lower level components) but across as well (pages perhaps are at the same level)

user state from logging in
->  information per page propagated by logging in
                -> information per component (state being propagated from start)

when creating a new deep component, figure out if it's a send info to db then do somthing (change page) or get info from db (or do nothing) and do something (send to children or nothing).  if it's getting stuff, it'll be from the propagation of the user state...
ex)plot component gets model version data from db.  But on customer journey once you get to component, we will have state: user, model name, model version, and it kept propagating.  This is how you KNOW what to ask the db for!!!  The store up to this point will have user, model etc
after getting info it presents graph to user.

2)interact with ui in some way

sign up ~ on a page, if you have input elements -> you know they will be binded to state.  if you have buttons you know it will run a method that will do something to the state
anything dynamic that you're displaying to a user is a state variable.  After fulfilling its purpose, the page uses frontend router to determine where to go

home page: not a page designed for sending info to backend, so it needs to get info instead.  Needs info to pass as props to child components ~ workflow design which alters ui

To know what the components are should be on a page and the components that determine numbers of other components, THINK ABOUT HOW PAGE WOULD DIFFER BASED ON DIFFERENT USERS and design flow ~ user journey through app
ex) after login page sends info and backend says this person is account1 and sends all their user info.  Now, what is shown at the model dashboard page is:
table element filled with entry static elements (or components... ~ if u want cells to be modifable in a unique way ex) instead of basic shit like changing color which can be passed in as a prop, the component makes an api request to change its color...) 
of the data given.  

From here, each cell can be a component that has a launch report button which when clicked changes frontend router.  This page depnding on user data stored in state
Again as a page = component it will either post/send and change page OR get data from db and decide to do something = give state to child components via props OR change pages
as a component that gets a state/prop passed into itself.  It can reinitialize itself based on props (switch on... = html + js + css) and sit there and do nothing (basic component/ html strcuture) OR it can GET/POST data to backend server ~ list of variables will init itself with user + model name + model version and then will get user, model name, version data from the backend! OR change pages
a component that gets data from backend can THEN DO: pass data to static components that it is made up of, OR dynamically create new components using this data  ex) multiple fb post components in the ccontent container component...

start with pages and info needed, which will be in store, you'll see how nested components will still rely on this higher level state!  ex) messenger button component which contains messages FROM a user 





-backend



*/

//What a feature means:
/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(
	fileUpload({
		limits: { fileSize: 50 * 1024 * 1024 },
	})
);
app.use(cors()); //allows resources exchange from different domains, ex) frontend to backend...
app.use(
	session({
		secret: 'secret',
		resave: true,
		saveUninitialized: true,
	})
); //for authentication purposes

// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production') {
	app.use(helmet());
}

// Add APIs
app.use('/api', BaseRouter);

// Print API errors
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
	console.log(err, true);
	return res.status(BAD_REQUEST).json({
		error: err.message,
	});
});

/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/
//delete?
// const viewsDir = path.join(__dirname, 'views');
// app.set('views', viewsDir);
// const staticDir = path.join(__dirname, 'public');
// app.use(express.static(staticDir));
// app.get('*', (req: Request, res: Response) => {
//     res.sendFile('index.html', {root: viewsDir});
// });

// Export express instance
export default app;
