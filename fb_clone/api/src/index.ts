const { API_PORT } = require('../config');
// import './pre-start'; // Must be the first import
import app from '@server';
// import logger from '@shared/Logger';

// Start the server
const port = Number(process.env.PORT || 3000);
console.log(`STARTING API ON PORT: ${API_PORT}`);

app.listen(port, () => {
	console.log('Express server started on port: ' + port);
});
