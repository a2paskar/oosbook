import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
var pool = require('../../postgres-db/pg-pool'); //need my db

const router = Router();
const { BAD_REQUEST, CREATED, OK } = StatusCodes;

/******************************************************************************
 *                      Get All posts - "GET /api/posts/all"
 ******************************************************************************/

//get all posts for a user
router.get('/:id', async (req: Request, res: Response) => {
	const user_id = req.params.id;

	const query = `SELECT post_id, user_id, author, content, profilepic, contentpic FROM posts WHERE user_id ='${user_id}' `;

	//get user's posts
	pool.query(query, (error: any, results: { rows: any }) => {
		if (error) {
			console.error('error:', error);
			res.send({ err: error });
		}
		if (results.rows.length > 0) {
			//match has been found
			console.log('Found posts: ', results.rows);

			res.send(results.rows);
			// res.redirect('/home'); //HOW DOES EXPRESS KNOW OF REACT/frontend routes???  It's not connected to the same index.html??  this shouooldn't work right...
		} else {
			console.log('No Posts Found!');
			res.send([]);
		}
		//for a backend to interact with a front end
		//it can send send data directly to where it was called (GET)
		//OR you can treat it as and independent network of routes + views (functions that respond to request at url ~defined in router..)
	});
});

/******************************************************************************
 *                      Get All posts for home page - "GET /api/posts/all"
 ******************************************************************************/

router.get('/all/:id', async (req: Request, res: Response) => {
	const user_id = req.params.id; //get id from dynamic url
	const friendsQuery = `SELECT friends FROM users WHERE user_id ='${user_id}' `;
	//get friends from users table
	pool.query(friendsQuery, (error: any, results: { rows: any }) => {
		if (error) {
			console.error('error:', error);
			res.send({ err: error });
		}
		if (results.rows.length > 0 && results.rows[0]['friends']) {
			//match has been found
			console.log('friends results: ', results.rows);
			const friends = results.rows[0]['friends'].split('*'); //friends are stored as user_ids concated via *
			friends.push(user_id);
			var query = `SELECT post_id, user_id, author, content, profilepic, contentpic FROM posts WHERE user_id in (`; //TODO: friend...
			friends.forEach((element: string, i: number) => {
				if (i == friends.length - 1) {
					return (query += "'" + element + "'" + ')');
				} else {
					return (query += "'" + element + "'" + ',');
				}
			});
			//get user's posts
			console.log('friends query: ', query);
			pool.query(query, (err: any, rez: { rows: any }) => {
				if (error) {
					console.error('error:', error);
					res.send({ err: err });
				}
				if (rez.rows.length > 0) {
					//match has been found
					console.log('Found all posts: ', rez.rows);
					//send all friends posts
					res.send(rez.rows);
				}
			});
		} else {
			console.log('No posts!');
			res.send({ status: 404 });
		}
	});
});

/******************************************************************************
 *                       Add One - "POST /api/posts/add"
 ******************************************************************************/

//user creates a post = saving it into posts table
router.post('/add', async (req: Request, res: Response) => {
	console.log('contentPICCC', req);

	const user_id = req.body.user_id;
	const content = req.body.content;
	const profilepic = req.body.profilepic;
	const author = req.body.author;

	var file = (req as any).files.file;
	var location =
		'/Users/atpouthanpaskaran/Documents/Main/FutureCareer/Programming/Projects/Web_apps/oosbook/oosbook-vue/database/users/content_pics/';
	location += `${user_id}.png`;
	file.mv(location, (err: any) => {
		if (err) {
			return res.status(500).send(err);
		}
		var save_location = '../../../../database/users/content_pics/';
		save_location += `${user_id}.png`;
		const query = `INSERT INTO posts(content, profilepic, contentpic, user_id, author) VALUES ('${content}','${profilepic}', '${save_location}', '${user_id}', '${author}')`;
		//use pg db tool to submit insert query
		pool.query(query, (error: any, results: { rows: any }) => {
			if (error) {
				console.error(error);
			} else {
				res.send({ results });
			}
		});
	});
});

/******************************************************************************
 *                       Update - "PUT /api/posts/update"
 ******************************************************************************/

//user updates their post ~ change row in db
router.put('/update', async (req: Request, res: Response) => {
	const user_id = req.body.user_id;
});

/******************************************************************************
 *                    Delete - "DELETE /api/posts/delete/:id"
 ******************************************************************************/

// user deletes their post
router.delete('/delete/:post_id', async (req: Request, res: Response) => {
	const { post_id } = req.params;

	const query = `DELETE FROM posts WHERE post_id = '${post_id}'`;
	//use pg db tool to submit insert query
	pool.query(query, (error: any, results: { rows: any }) => {
		if (error) {
			console.error(error);
		} else {
			res.send({ results });
		}
	});
});

export default router;
