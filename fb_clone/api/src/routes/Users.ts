import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
var pool = require('../../postgres-db/pg-pool');

const router = Router();
const { BAD_REQUEST, CREATED, OK } = StatusCodes;

// /******************************************************************************
//  *                      Get All Users - "GET /api/users/all"
//  ******************************************************************************/

// router.get('/all', async (req: Request, res: Response) => {
//     const users = await userDao.getAll();
//     return res.status(OK).json({users});
// });

// /******************************************************************************
//  *                       Add One - "POST /api/users/add"
//  ******************************************************************************/

// router.post('/add', async (req: IRequest, res: Response) => {
//     const { user } = req.body;
//     if (!user) {
//         return res.status(BAD_REQUEST).json({
//             error: paramMissingError,
//         });
//     }
//     await userDao.add(user);
//     return res.status(CREATED).end();
// });

// /******************************************************************************
//  *                       Update - "PUT /api/users/update"
//  ******************************************************************************/

// router.put('/update', async (req: IRequest, res: Response) => {
//     const { user } = req.body;
//     if (!user) {
//         return res.status(BAD_REQUEST).json({
//             error: paramMissingError,
//         });
//     }
//     user.id = Number(user.id);
//     await userDao.update(user);
//     return res.status(OK).end();
// });

// /******************************************************************************
//  *                    Delete - "DELETE /api/users/delete/:id"
//  ******************************************************************************/

// router.delete('/delete/:id', async (req: IRequest, res: Response) => {
//     const { id } = req.params;
//     await userDao.delete(Number(id));
//     return res.status(OK).end();
// });

/* GET users listing. */

router.get(
	'/isFriends/:friend_id/:user_id',
	async (req: Request, res: Response) => {
		const friend_id = req.params.friend_id; //get id from dynamic url
		const user_id = req.params.user_id;
		const friendsQuery = `SELECT friends FROM users WHERE user_id ='${user_id}' `;
		//get friends from users table
		pool.query(friendsQuery, (error: any, results: { rows: any }) => {
			if (error) {
				console.error('error:', error);
				res.send({ err: error });
			}
			if (results.rows.length > 0 && results.rows[0]['friends']) {
				//match has been found

				const friends = results.rows[0]['friends'].split('*'); //friends are stored as user_ids concated via *

				var isFriend = friends.some(function (el: string) {
					console.log(el);
					return el == friend_id;
				});
				console.log('isFriend: ', isFriend);
				res.send(isFriend);
			} else {
				console.log('No friends!');
				res.send(false);
			}
		});
	}
);

router.get('/wallpaperdata/:id', function (req, res, next) {
	const id = req.params.id;

	pool.query(
		`SELECT firstname, lastname, profilepic, wallpaper FROM users WHERE user_id = '${id}' `,
		(error: any, results: { rows: any }) => {
			if (error) {
				console.error(error);
			}
			if (typeof results !== 'undefined') {
				res.send(results.rows);
			}
		}
	);
});

// router.get('/profilepic/:id', function (req, res, next) {
// 	const id = req.params.id;
// 	console.log('WALLPAPER');

// 	pool.query(
// 		`SELECT profilepic FROM users WHERE user_id = '${id}' `,
// 		(error: any, results: { rows: any }) => {
// 			if (error) {
// 				console.error(error);
// 			}
// 			console.log('FOUND: ', results);
// 			if (typeof results !== 'undefined') {
// 				res.send(results.rows);
// 			}
// 		}
// 	);
// });

router.post('/wallpaper/:id', function (req, res, next) {
	const id = req.params.id;
	var file = (req as any).files.file;
	var location =
		'/Users/atpouthanpaskaran/Documents/Main/FutureCareer/Programming/Projects/Web_apps/oosbook-vue/database/users/wall_paper/';
	location += `${id}.png`;
	file.mv(location, (err: any) => {
		if (err) {
			return res.status(500).send(err);
		}
		var save_location = '../../../../database/users/wall_paper/';
		save_location += `${id}.png`;
		pool.query(
			`UPDATE users SET wallpaper='${save_location}' WHERE user_id ='${id}'`,
			(error: any, results: { rows: any }) => {
				if (error) {
					console.error(error);
				}
				if (typeof results !== 'undefined') {
					res.send(results.rows);
				}
			}
		);
	});
});

router.post('/profilepic/:id', function (req, res, next) {
	const id = req.params.id;
	var file = (req as any).files.file;
	var location =
		'/Users/atpouthanpaskaran/Documents/Main/FutureCareer/Programming/Projects/Web_apps/oosbook/oosbook-vue/database/users/profile_pics/';
	location += `${id}.png`;
	file.mv(location, (err: any) => {
		if (err) {
			return res.status(500).send(err);
		}
		var save_location = '../../../../database/users/profile_pics/';
		save_location += `${id}.png`;
		pool.query(
			`UPDATE users SET profilepic='${save_location}' WHERE user_id ='${id}'`,
			(error: any, results: { rows: any }) => {
				if (error) {
					console.error(error);
				}
				if (typeof results !== 'undefined') {
					res.send(results.rows);
				}
			}
		);
	});
});

router.get('/:search', function (req, res, next) {
	const search = req.params.search;
	console.log('User is searching up: ', search);

	pool.query(
		`SELECT user_id, firstname, lastname, profilepic FROM users WHERE firstname LIKE '%${search}%' OR lastname LIKE '%${search}%'`,
		(error: any, results: { rows: any }) => {
			if (error) {
				console.error(error);
			}
			res.send(results.rows);
		}
	);
});
router.get('/profilepic/:id', function (req, res, next) {
	const id = req.params.id;

	pool.query(
		`SELECT profilepic FROM users WHERE user_id = '${id}' `,
		(error: any, results: { rows: any }) => {
			if (error) {
				console.error(error);
			}
			if (typeof results !== 'undefined') {
				res.send(results.rows);
			}
		}
	);
});
router.post('/register', function (req, res, next) {
	const firstName = req.body.firstName;
	const lastName = req.body.lastName;
	const email = req.body.email;
	const password = req.body.password;
	const gender = req.body.gender;

	const query = `INSERT INTO users( password, email, firstName, lastName, gender) VALUES ('${password}','${email}', '${firstName}', '${lastName}', '${gender}')`;
	//use pg db tool to submit insert query
	pool.query(query, (error: any, results: { rows: any }) => {
		if (error) {
			console.error('error:', error);
		} else {
			res.send({ results });
		}
	});
});
router.post('/login', function (req, res, next) {
	const password = req.body.password;
	const email = req.body.email;

	const query = `SELECT user_id, firstname, lastname, gender, email, password FROM users WHERE email ='${email}' AND password ='${password}'`;
	pool.query(query, (error: any, results: { rows: any }) => {
		if (error) {
			console.error('error:', error);
			res.send({ err: error });
		}
		if (results.rows.length > 0) {
			//match has been found
			console.log('Found!', results.rows);
			const data = {
				firstname: results.rows[0]['firstname'],
				lastname: results.rows[0]['lastname'],
				gender: results.rows[0]['gender'],
				email: results.rows[0]['email'],
				password: results.rows[0]['password'],
				user_id: results.rows[0]['user_id'],
			};

			res.send(data);
			// res.redirect('/home'); //HOW DOES EXPRESS KNOW OF REACT/frontend routes???  It's not connected to the same index.html??  this shouooldn't work right...
		}
		//for a backend to interact with a front end
		//it can send send data directly to where it was called (GET)
		//OR you can treat it as and independent network of routes + views (functions that respond to request at url ~defined in router..)
	});
});

//   pool.query('INSERT INTO users( password, email, first_name, last_name, date_of_birth, gender) VALUES (?,?, ?, ?, ?, ?)',[ password, email, first_name, last_name, date_of_birth, gender] , (error: any, results: { rows: any; }) => {
//     if (error) {
//         console.error('error:', error);
//     };
// });

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
