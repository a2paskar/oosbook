import dotenv from 'dotenv';
dotenv.config();
module.exports = {
    NODE_ENV: process.env.NODE_ENV,
    API_PORT: process.env.PORT,
    DB_URL: process.env.DB_URL,
    DB_CONNECTION_VARS: {
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        name: process.env.DB_NAME,
        port: process.env.DB_PORT,
    },
};