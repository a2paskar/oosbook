<h1> Simplified Clone of Facebook Web Application </h1>

<h3>Technologies include: VueJS + Vuex, Express (NodeJS), PostgreSQL </h3>

<p>
    Implemented features include:  
</p>
<ul>
    <li> Login and Registration Page  </li>
    <li> Home Page  </li>
    <li> Profile Page </li>
    <li> Search for New Friends </li>
    <li> Modeling friendships in database structure </li>
    <li> Storing and retrieving user uploaded images for posts, wallpaper, profile pictures </li>
    <li> User restriction for certain pages and components (Ex) a user cannot change the profile picture of another user)</li>
</ul>
